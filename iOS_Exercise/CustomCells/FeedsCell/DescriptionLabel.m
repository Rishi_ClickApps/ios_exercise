//
//  DescriptionLabel.m
//  JustNow
//
//  Created by apple on 17/08/15.
//  Copyright (c) 2015 ClickApps. All rights reserved.
//

#import "DescriptionLabel.h"

@implementation DescriptionLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds);
    
    [super layoutSubviews];
}

@end
