//
//  FeedsDetailVC.h
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedsDetailVC : UIViewController

/**
 @property feedsDictionary
 @description Dictionary holding feeds detail
 */
@property (strong, nonatomic) NSDictionary *feedsDictionary;
@end
