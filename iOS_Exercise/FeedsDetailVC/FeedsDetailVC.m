//
//  FeedsDetailVC.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "FeedsDetailVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define kTITTLEPADDINGTOP 15.0
#define kTITTLEPADDINGBOTTOM 15.0
#define kBOTTOMPADDING  20.0

@interface FeedsDetailVC ()

/**
 @property lblTittle
 @description Tittle for the feed
 */
@property (weak, nonatomic) IBOutlet UILabel *lblTittle;

/**
 @property lblDescription
 @description Description for the feed
 */
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

/**
 @property imgvFeedImage
 @description UIImageView for the feed
 */
@property (weak, nonatomic) IBOutlet UIImageView *imgvFeedImage;

/**
 @property verticalTopConstraint
 @description Constarint to handle vertical top space of tittle lable
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalTopConstraint;


/**
 @property heightScrollConstraint
 @description Constarint to handle scroll height
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightScrollConstraint;
@end

@implementation FeedsDetailVC

#pragma mark - View LifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTittle.text = [self.feedsDictionary valueForKey:@"title"];
    self.lblDescription.text = [self.feedsDictionary valueForKey:@"feedsdescription"];
    self.imgvFeedImage.clipsToBounds = YES;
    
    float heightToAdd = ([UIScreen mainScreen].bounds.size.height/100)*25;
    if ([[self.feedsDictionary valueForKey:@"image"] length] > 0) {
        [self.imgvFeedImage sd_setImageWithURL:[NSURL URLWithString:[self.feedsDictionary valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"No_Image_Placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image){
                self.imgvFeedImage.image = image;
            }
        }];
    }
    else {
         self.verticalTopConstraint.constant = -heightToAdd + 10.0 ;
         heightToAdd = 0.0;
    }
    self.heightScrollConstraint.constant =  heightToAdd + [self heightForLabel:self.lblDescription andWidthGap:16.0] + kTITTLEPADDINGTOP + self.lblTittle.frame.size.height + kTITTLEPADDINGTOP + kBOTTOMPADDING;
}

#pragma mark - Memory Management Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(float)heightForLabel:(UILabel *)lable andWidthGap:(float)widthGap {
    CGRect textRect = [lable.text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - widthGap, MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin  attributes:@{NSFontAttributeName:lable.font} context:nil];
    CGSize size = textRect.size;
    return  size.height;
}

/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
