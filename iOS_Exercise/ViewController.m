//
//  ViewController.m
//  iOS_Exercise
//
//  Created by Rishi Ghosh Roy on 11/22/16.
//  Copyright © 2016 ClickApps. All rights reserved.
//

#import "ViewController.h"
#import "FeedsCell.h"
#import "Constant.h"
#import "Feeds.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import <RKEntityMapping.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "FeedsDetailVC.h"
#import "AFHTTPClient.h"
#import <RMUniversalAlert/RMUniversalAlert.h>
#import "RKiOS7Loading.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
    /**
     @var feedsArray
     @description Array to store feed response received from the api
     */
    NSMutableArray *feedsArray;
    
    __block BOOL isReachable;
    
    RKObjectManager *objectManager;
}

/**
 @property tblFeeds
 @description UITableView to show feeds
 */
@property (weak, nonatomic) IBOutlet UITableView *tblFeeds;


@end

@implementation ViewController

#pragma mark - View LifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupInitialLayoutSettings];
}

#pragma mark - Other Methods

/**
 @method setupInitialLayoutSettings
 @description Method to set initial layout settings and default values
 */
- (void)setupInitialLayoutSettings {
    
    isReachable = TRUE;
    //Get the reference of the RKObjectManager class, configured with the BASE URL
    objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [objectManager.HTTPClient setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable) {
            
            isReachable = FALSE;
        }
    }];
    
    UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:self action:@selector(refreshData)];
    self.navigationItem.rightBarButtonItem = btnRefresh;
    
    self.tblFeeds.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    //Register cell for tableView
    [self.tblFeeds registerNib:[UINib nibWithNibName:@"FeedsCell" bundle:nil] forCellReuseIdentifier:@"FeedCellIdentifier"];
    [self checkIfFeedsAlreadyAvailable];
}

/**
 @method checkIfFeedsAlreadyAvailable
 @description This method will check if feeds files is already downloaded in document directory or not
 */
- (void)checkIfFeedsAlreadyAvailable {
    
    //Check if file exists in document directory or not
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"]];
    if (!fileExists) {
        
        if (isReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [RKiOS7Loading showHUDToView:self.view animated:YES];
            });
            
            [self downloadFile];
        }
        else {
            
            [RMUniversalAlert showAlertInViewController:self withTitle:@"No network connection" message:@"You must be connected to the internet to use this app." cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
            }];
        }
    }
    else {
        [self mapDownloadedFeeds];
    }
}

/**
 @method refreshData
 @description Method to get updated response from api
 */
- (void)refreshData {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"] error:&error];
        if (success) {
            
            [self checkIfFeedsAlreadyAvailable];
        }
    });
}

/**
 @method getDocumentDirectoryReference
 @description Method to get the document directory reference path
 @returns Path of the document directory
 */
- (NSString *)getDocumentDirectoryReference {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

/**
 @method downloadFile
 @description This method will download the json file from the url and save into the document directory
 */
- (void)downloadFile {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:BASE_URL]];
    AFHTTPRequestOperation *downloadRequest = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [downloadRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //If response received is succes,write the response in to the document directory path
        if (responseObject) {
            
            NSData *data = [[NSData alloc] initWithData:responseObject];
            NSString *filePath = [[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"];
            NSLog(@"File path is %@",filePath);
            [data writeToFile:filePath atomically:YES];
            
            [self mapDownloadedFeeds];
        }
        else {
            NSLog(@"Unable to download file");
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"file downloading error : %@", [error localizedDescription]);
    }];
    
    // Step 5: begin asynchronous download
    [downloadRequest start];
}

/**
 @method mapDownloadedFeeds
 @description This method will start the Mapping process and write the data into the core data instance
 */
- (void)mapDownloadedFeeds {
    
    
    //Get the downloaded file from the document directory for initiating the mapping process
    NSString *feedsResponse = [[NSString alloc] initWithContentsOfFile:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"] encoding:NSUTF8StringEncoding error:NULL];
    NSString *MIMEType = @"application/json";
    NSError *parsingError = nil;
    
    //Convert the data into the JSON object with encoding
    NSData *data = [feedsResponse dataUsingEncoding:NSUTF8StringEncoding];
    id parsedData = [RKMIMETypeSerialization objectFromData:data MIMEType:MIMEType error:&parsingError];
    if (parsedData == nil && parsingError) {
        
        [RMUniversalAlert showAlertInViewController:self withTitle:@"Error" message:@"Unable to parse the response." cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
            
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [RKiOS7Loading hideHUDForView:self.view animated:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [RKiOS7Loading hideHUDForView:self.view animated:YES];
        });
    }
    
    
    //Get the reference on the managed object instance and assign to RKObjectManager
    NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FeedsModel" ofType:@"momd"]];
    NSManagedObjectModel *managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    objectManager.managedObjectStore = managedObjectStore;
    
    //Start the mapping process with reference to the model class
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"Feeds" inManagedObjectStore:managedObjectStore];
    [feedMapping addAttributeMappingsFromDictionary:@{
                                                      @"title": @"title",
                                                      @"description": @"feedsdescription",
                                                      @"imageHref": @"image"
                                                      }];
    
    
    NSString *seedPath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"FeedsModel.sqlite"];
    RKManagedObjectImporter *importer = [[RKManagedObjectImporter alloc] initWithManagedObjectModel:managedObjectStore.managedObjectModel storePath:seedPath];
    
    NSError *error;
    [importer importObjectsFromItemAtPath:[[self getDocumentDirectoryReference] stringByAppendingPathComponent:@"feeds.json"]
                              withMapping:feedMapping
                                  keyPath:@"rows"
                                    error:&error];
    //Check if response is successfully written into the code data instance or not
    BOOL success = [importer finishImporting:&error];
    
    if (success) {
        
        [importer logSeedingInfo];
    }
    else {
        
        NSLog(@"error localized description == %@", [error localizedDescription]);
    }
    
    feedsArray = [NSMutableArray new];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"title" ascending:YES];
    NSArray *sortedArray = [[self getFeedsFromDatabase] sortedArrayUsingDescriptors:@[sortDescriptor]];
    feedsArray = [NSMutableArray arrayWithArray:sortedArray];
    [self.tblFeeds reloadData];
}
/**
 @method getFeedsDatabase
 @description This method will get the saved feed instances from the core data manage object context
 @returns Returns the array of the feeds from the db
 */

- (NSMutableArray *)getFeedsFromDatabase {
    
    NSMutableArray *recordArray = [NSMutableArray new];
    NSManagedObjectContext *managedObjectContext = [APP_DELEGATE managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Feeds"];
    
    NSArray *array = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *records in array) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:0];
        [dict setValue:[records valueForKey:@"title"] forKey:@"title"];
        [dict setValue:[records valueForKey:@"feedsdescription"] forKey:@"feedsdescription"];
        [dict setValue:[records valueForKey:@"image"] forKey:@"image"];
        if ([[records valueForKey:@"title"] length] == 0 && [[records valueForKey:@"feedsdescription"] length] == 0 && [[records valueForKey:@"image"] length] == 0) {
            continue;
        }
        [recordArray addObject:dict];
    }
    return recordArray;
}


#pragma mark - UITableViewDataSource & Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [feedsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"FeedCellIdentifier";
    
    FeedsCell *cell = (FeedsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"FeedsCell" owner:self options:nil] objectAtIndex:0];
    }
    
    if ([feedsArray count] > 0) {
        
        cell.lblFeedTittle.text = [[feedsArray objectAtIndex:indexPath.row] valueForKey:@"title"];
        cell.lblFeedDescription.text = [[feedsArray objectAtIndex:indexPath.row] valueForKey:@"feedsdescription"];
        cell.descriptionLabelTrailingConstraint.constant = 0;
        if ([[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"] length] > 0) {
            cell.descriptionLabelTrailingConstraint.constant = 87;
        }
        cell.imgVwFeedImage.hidden  = YES;
        if ([[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"] length] > 0) {
            cell.imgVwFeedImage.hidden  = NO;
            [cell.imgVwFeedImage sd_setImageWithURL:[NSURL URLWithString:[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"No_Image_Placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image){
                    cell.imgVwFeedImage.image = image;
                }
            }];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FeedsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedCellIdentifier"];
    
    if (cell == nil) {
        
        cell = (FeedsCell *)[[[NSBundle mainBundle] loadNibNamed:@"FeedsCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.lblFeedDescription.text = [[feedsArray objectAtIndex:indexPath.row] valueForKey:@"feedsdescription"];
    cell.descriptionLabelTrailingConstraint.constant = 0;
    if ([[[feedsArray objectAtIndex:indexPath.row] valueForKey:@"image"] length] > 0) {
        cell.descriptionLabelTrailingConstraint.constant = 87;
    }
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(cell.bounds), CGRectGetHeight(cell.bounds));
    [cell setNeedsLayout];
    
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    if (size.height < 101.0) {
        return 101;
    }
    else
        return size.height + 1.0f; // Add 1.0f for the cell separator height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedsDetailVC *feedDetail = (FeedsDetailVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"FeedsDetailVC"];
    feedDetail.feedsDictionary = [feedsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:feedDetail animated:YES];
    
}

#pragma mark - Memory Management Methods

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
